from turtle import *
import random
speed(speed='fastest')
def draw(n,x,angle):
#Loop for Number of stars
    for i in range(n):
        colormode(255)
#Choosing random integers between 0 and 255 to generate random RGB values
        a = random.randint(0, 255)
        b = random.randint(0, 255)
        c = random.randint(0, 255)
#Setting the Outline and fill color
        pencolor(a, b, c)
        fillcolor(a, b, c)
#Begin filling the stars
        begin_fill()
#Loop for drawing each star
        for j in range(5):
            forward(5*n-5*i)
            right(x)
            forward(5*n-5*i)
            right(72-x)
#color filling complete
        end_fill()
#rotating for the next star
        rt(angle)
    Screen().exitonclick()
#setting parameters
n=30 #number of stars
x=144 #exterior angle of each star
angle=18 #angle of rotation for the spiral
draw(n,x,angle)